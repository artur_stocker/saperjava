package saperjava.controller;

import saperjava.model.Game;
import saperjava.model.GameSettings;
import saperjava.view.IMainWindow;

/**
 * Interface of controller used by GUI.
 */
public interface IGameController
{
	void setGame(Game game);
	void setMainWindow(IMainWindow mainWindow);

	void doSquareAction(int x, int y, int button);

	void startNewGame();
	void startNewGame(GameSettings gameSettings);

	void showLeaderboard();
}
