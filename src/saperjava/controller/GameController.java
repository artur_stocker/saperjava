package saperjava.controller;

import saperjava.model.GameSettings;
import saperjava.model.Game;
import saperjava.model.Square;
import saperjava.model.observer.Observer;
import saperjava.view.IMainWindow;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Template for game controller object.
 */
public class GameController implements IGameController, Observer
{
	/** Reference to game object. */
	private Game game;
	/** Method for setting reference to game object. */
	public void setGame(Game game) {
		this.game = game;
		this.game.registerObserver(this);
	}

	/** Reference to GUI object. */
	private IMainWindow mainWindow;
	/** Method for setting reference to GUI object. */
	public void setMainWindow(IMainWindow mainWindow) {
		this.mainWindow = mainWindow;
		startNewGame();
	}

	/** Game settings field. */
	private GameSettings gameSettings = new GameSettings();

	/** Method for starting new game with previous game settings */
	public void startNewGame(){
		newGame();
	}

	/**
	 * Method for starting new game with new game settings.
	 *
	 * @param gameSettings Game settings for new game.
	 */
	public void startNewGame(GameSettings gameSettings){
		this.gameSettings = gameSettings;
		newGame();
	}

	/**
	 * Field that counts number of mines - flags for active game.
	 * Can have negative values.
	 */
	private int mines = 0;

	/** Method that setting new game for start. */
	private void newGame(){
		// Game part.
		game.startNewGame(this, gameSettings);

		// GUI part.
		mainWindow.createNewGame(this.gameSettings.getRows(), this.gameSettings.getCols());
		mainWindow.setTimerTime(0);
		mainWindow.setMinesText(this.gameSettings.getMines());

		// Controller part.
		mines = this.gameSettings.getMines();

		firstClick = true;
		if (timer.isRunning()) timer.stop();
	}

	/** Flag that informs about first click for current game. */
	private boolean firstClick;

	/**
	 * Method that process signals from GUI and do corresponding action on game model.
	 * Method is provided for GUI object by Interface.
	 *
	 * @param x x position
	 * @param y y position
	 * @param button number of button that was pressed
	 */
	@Override
	public synchronized void doSquareAction(int x, int y, int button) {
		if (game.getStatus() != Game.Status.GOING) return;
		if (firstClick) startTimer();

		Game.Action action = null;
		switch (button){
			// Left button.
			case MouseEvent.BUTTON1:
				action = Game.Action.REVEAL;
				break;
			// Middle button.
			case MouseEvent.BUTTON2:
				action = Game.Action.CHECK_FLAG;
				break;
			// Right button.
			case MouseEvent.BUTTON3:
				action = Game.Action.FLAG;
				break;
		}

		game.doActionOnSquare(x, y, action);
	}

	/** Timer for counting time. */
	private Timer timer = new Timer(1000, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			time++;
			mainWindow.setTimerTime(time);
		}
	});
	/** Current game time in seconds. */
	private int time = 0;

	/** Method for start timer with reset of current game time. */
	private void startTimer(){
		firstClick = false;
		time = 0;

		timer.start();
	}
	/** Method for stop timer with reset of first click flag. */
	private void stopTimer(){
		firstClick = true;

		timer.stop();
	}

	// Observer pattern.
	/** Implementation of observer for game status changes. */
	@Override
	public void updateGameStatus(Game.Status status) {
		switch (status){
			case WIN:
				stopTimer();
				saveLeaderboards(mainWindow.winWindow());
				break;

			case LOSE:
				stopTimer();
				mainWindow.loseWindow();
				break;
		}
	}

	/** Implementation of observer for squares status changes. */
	@Override
	public void updateSquareStatus(int x, int y, Square.Status status, int n) {
		mainWindow.changeSquare(x, y, status, n);

		switch (status){
			case FLAG:
				mines--;
				break;
			case CLEAR:
				mines++;
		}
		mainWindow.setMinesText(mines);
	}

	/** Method for call GUI leaderboard. */
	@Override
	public void showLeaderboard() {
		mainWindow.showLeaderboard(loadLeaderboards());
	}

	/** Leaderboard file path and name. */
	private String leaderboardFile = "leaders.txt";

	/**
	 * Method for saving a new record in leaderboard file. Overrides the leaderboard by add record at the end of file
	 * or create new one if not exist.
	 * Reading information about game from game setting object.
	 *
	 * @param name Name of player.
	 */
	private void saveLeaderboards(String name){
		try{
			if (name.equals("")) return;
			BufferedWriter writer;

			writer = new BufferedWriter(new FileWriter( leaderboardFile, true ));

			writer.write(
				String.format("%s %s %d",name, printSize() ,time) +
				System.lineSeparator()
			);

			writer.close();
		} catch (Exception e)
		{
			System.out.println("IOException during saving leaders file!");
		}
	}

	/**
	 * Method that determine description of game size for current game.
	 *
	 * @return Description of game size.
	 */
	private String printSize(){
		String size;

		if (gameSettings.getRows() == 9 &&
				gameSettings.getCols() == 9 &&
				gameSettings.getMines() == 10)
			size = "Beginner";
		else if (gameSettings.getRows() == 16 &&
				gameSettings.getCols() == 16 &&
				gameSettings.getMines() == 40)
			size = "Intermediate";
		else if (gameSettings.getRows() == 16 &&
				gameSettings.getCols() == 30 &&
				gameSettings.getMines() == 99)
			size = "Advanced";
		else
			size = String.format(
					"Custom(%dx%dx%d)",
					gameSettings.getRows(),
					gameSettings.getCols(),
					gameSettings.getMines()
			);

		return size;
	}


	/**
	 * Method that is loading records from leaderboards file.
	 *
	 * @return List of records information.
	 * @see saperjava.view.IMainWindow.LeaderboardInfo
	 */
	private List<IMainWindow.LeaderboardInfo> loadLeaderboards(){
		List<IMainWindow.LeaderboardInfo> infos = new ArrayList<>();

		try {
			Scanner scanner = new Scanner( new File(leaderboardFile));

			while (scanner.hasNextLine()){
				String name = scanner.next();
				String size = scanner.next();
				int time = scanner.nextInt();
				infos.add(new IMainWindow.LeaderboardInfo(name, size, time));
			}
			scanner.close();
		} catch (Exception e)
		{
			System.out.println("IOException during loading leaders file!");
		}

		if (infos.isEmpty()) return null;
		return infos;
	}
}
