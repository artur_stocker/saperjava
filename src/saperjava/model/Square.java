package saperjava.model;

import saperjava.model.observer.Observer;
import saperjava.model.observer.Subject;
import java.util.ArrayList;
import java.util.List;

/** Template for Square object. */
public class Square implements Subject
{
	private int x;
	int getX() { return x; }

	private int y;
	int getY() { return y; }

	/**
	 * Constructor creating Square object and setting its coordinates.
	 *
	 * @param x X coordinate
	 * @param y Y coordinate
	 */
	Square(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/** Flag for having flag by square. */
	private boolean flag = false;
	/** Method that changes flag status. */
	void alternateFlag() {
		if (checked) return;
		flag = !flag;
		status = (flag) ? Status.FLAG : Status.CLEAR;
		notifyObservers();
	}
	boolean haveFlag() { return flag; }

	/** Flag for having mine by square. */
	private boolean mine = false;
	void setMine() { mine = true; }
	boolean haveMine() { return mine; }

	/** Flag for being checked for square. */
	private boolean checked = false;
	void setChecked() {
		checked = true;
		status = (!mine) ? Status.CHECKED : Status.MINE;
		notifyObservers();
	}
	boolean isChecked() { return checked; }

	/** Number of nearby mines. */
	private int nearbyMines = 0;
	void setNumberOfNearbyMines(int number) { nearbyMines = number; }
	int getNumberOfNearbyMines() { return nearbyMines; }

	/** List of references to squares that are adjacent to this square. */
	private List<Square> adjacentSquares = new ArrayList<>();
	void addAdjacentSquare(Square square) { adjacentSquares.add(square); }
	List<Square> getAdjacentSquares() { return adjacentSquares; }

	// Status
	/** Enum - status for notify observers. */
	public enum Status {CLEAR, FLAG, CHECKED, MINE}
	/** Square status. */
	private Status status = Status.CLEAR;


	// Observer pattern.
	/** List of observers. */
	private List<Observer> observers = new ArrayList<Observer>();

	/**
	 * Implementation of Subject for square.
	 * @see Subject
	 */
	@Override
	public void registerObserver(Observer observer) {
		this.observers.add(observer);
	}

	/**
	 * Implementation of Subject for square.
	 * @see Subject
	 */
	@Override
	public void unregisterObserver(Observer observer) {
		this.observers.remove(observer);
	}

	/**
	 * Implementation of Subject for square.
	 * @see Subject
	 */
	@Override
	public void notifyObservers() {
		for (Observer o: observers){
			int n = 0;
			if (status == Status.CHECKED) n = nearbyMines;
			o.updateSquareStatus(x, y, status, n);
		}
	}
}
