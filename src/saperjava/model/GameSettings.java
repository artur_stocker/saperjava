package saperjava.model;

/** Template for GameSetting Object. */
public class GameSettings
{
	public static final int MIN_ROWS = 9;
	public static final int MAX_ROWS = 40;

	public static final int MIN_COLS = 9;
	public static final int MAX_COLS = 60;

	public static final int MIN_MINES = 10;
	public static final double MAX_MINES_MULTIPLIER = 0.9;
	/**
	 * Method that allows to calculate maximum number of mines for this GameSettings.
	 * @return Maximum number of mines.
	 */
	public int calcMaxMines() { return (int)(rows * cols * MAX_MINES_MULTIPLIER); }

	private int rows = MIN_ROWS;
	public void setRows(int x) {
		if (x < MIN_ROWS) rows = MIN_ROWS;
		else rows = Math.min(x, MAX_ROWS);
		mines = MIN_MINES;
	}
	public int getRows() { return rows; }

	private int cols = MIN_COLS;
	public void setCols(int x) {
		if (x < MIN_COLS) cols = MIN_COLS;
		else cols = Math.min(x, MAX_COLS);
		mines = MIN_MINES;
	}
	public int getCols() { return cols; }

	private int mines = MIN_MINES;
	public void setMines(int x) {
		if (x < MIN_MINES) mines = MIN_MINES;
		else mines = Math.min(x, calcMaxMines());
	}
	public int getMines() { return mines; }

	// Builder pattern.
	/** Constructor that create GameSetting object. */
	public GameSettings(){}

	/** Private constructor for use by internal builder class.
	 * @see Builder
	 */
	private GameSettings(Builder builder){
		setRows(builder.rows);
		setCols(builder.cols);
		setMines(builder.mines);
	}

	/**
	 * Builder for GameSettings.
	 * Internal class for easier creation of new GameSetting objects.
	 */
	public static class Builder
	{
		private int rows;
		private int cols;
		private int mines;

		public Builder setRows(int rows){
			this.rows = rows;
			return this;
		}

		public Builder setCols(int cols){
			this.cols = cols;
			return this;
		}

		public Builder setMines(int mines){
			this.mines = mines;
			return this;
		}

		public GameSettings build(){
			return new GameSettings(this);
		}
	}
}
