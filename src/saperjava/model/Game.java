package saperjava.model;

import saperjava.model.observer.Observer;
import saperjava.model.observer.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/** Template for Game object. */
public class Game implements Subject
{
	/** Game settings field. */
	private GameSettings gameSettings;

	/** Reference to Observer object. */
	private Observer observer;
	/** List of square objects. */
	private List<Square> squares = new ArrayList<>();

	// Game creation part.

	/**
	 * Method that starts new game.
	 *
	 * @param observer Reference to Observer.
	 * @param gameSettings Reference to GameSetting.
	 *
	 * @see Observer
	 * @see GameSettings
	 */
	public void startNewGame(Observer observer, GameSettings gameSettings) {
		try
		{
			this.observer = observer;
			this.gameSettings = gameSettings;

			setGameStatus();
		}
		catch (Exception e)
		{
			System.out.println("Exception during starting new game!");
		}
	}

	/** Method that prepare all statuses for new game. */
	private void setGameStatus() {
		status = Status.GOING;

		setNumberOfSquaresToReveal();

		revealedSquares = 0;
		firstClick = false;
	}

	// Setting new board.
	/** Method that prepare new board by clearing old one and setting new squares according to game settings. */
	private void setNewBoard(int x, int y) {
		// Delete old board.
		squares.clear();

		// Create new squares.
		for (int i = 0; i < gameSettings.getCols(); i++){
			for (int j = 0; j < gameSettings.getRows(); j++)
			{
				squares.add(new Square(i+1, j+1));
			}
		}

		// Add references to squares about adjacent to them squares.
		setAdjacentSquares();
		// Add mines on random squares.
		setMines(gameSettings.getMines(), x, y);
		// Set numbers of nearby mines on squares.
		setNearbyMines();

		// Set observer for squares.
		setSquaresObserver(observer);
	}

	/** Method that set reference of observer for squares. */
	private void setSquaresObserver(Observer observer){
		for (Square s: squares)
		{
			s.registerObserver(observer);
		}
	}

	/** Method that set given amount of mines to squares.
	 * Checks for first click in new game.
	 *
	 * @param numOfMines number of mines to put on board
	 * @param x X coordinate of first click
	 * @param y Y coordinate of first click
	 */
	private void setMines(int numOfMines, int x, int y) {
		List<Square> copy = new LinkedList<>(squares);
		Collections.shuffle(copy);
		for (Square s: copy.subList(0, numOfMines)) {
			// Preventing first turn lose.
			if (x == s.getX() && y == s.getY()) {
				copy.get(numOfMines).setMine();
				continue;
			}
			s.setMine();
		}
	}

	/** Method that set number of nearby mines to squares. */
	private void setNearbyMines() {
		for (Square s: squares)
		{
			int i = 0;

			for (Square sx: s.getAdjacentSquares())
			{
				if (sx.haveMine()) i++;
			}

			s.setNumberOfNearbyMines(i);
		}
	}

	/** Method that set adjacent squares to squares. */
	private void setAdjacentSquares() {
		for (Square s: squares)
		{
			for (int x = s.getX() - 1; x <= s.getX() + 1; x++) {
			for (int y = s.getY() - 1; y <= s.getY() + 1; y++)
			{
				if (isInBoard(x, y) && !(x == s.getX() && y == s.getY())) {
					s.addAdjacentSquare(getSquare(x, y));
				}
			}}
		}
	}

	/** Method to get certain square from list of squares. */
	private Square getSquare(int x, int y) {
		for (Square s: squares)
		{
			if(x == s.getX() && y == s.getY()) return s;
		}
		return null;
	}

	// Game status part.
	/** Enum - status of game for notify observers. */
	public enum Status { WIN, GOING, LOSE }
	/** Flag of game status. */
	private Status status;
	public Status getStatus() { return status; }

	/** Number of squares to reveal for wining condition. */
	private int numberOfSquaresToReveal = 0;
	private void setNumberOfSquaresToReveal(){
		numberOfSquaresToReveal =
				(gameSettings.getRows() * gameSettings.getCols()) - gameSettings.getMines();
	}
	/** Number of revealed squares. */
	private int revealedSquares;
	/** Method that check win condition.
	 * @return true if win condition was met.
	 */
	private boolean checkWinCondition(){
		return revealedSquares == numberOfSquaresToReveal;
	}

	/** Method that checks if coordinates are on board range.
	 * @return true if is on board.
	 */
	private boolean isInBoard(int x, int y){
		return 	0 < x &&
				x <= gameSettings.getCols() &&
				0 < y &&
				y <= gameSettings.getRows();
	}

	// Gameplay part.
	/** Enum - Action of "moves". */
	public enum Action { REVEAL, FLAG, CHECK_FLAG }

	/** Flag for first move in current game. */
	private boolean firstClick;

	/**
	 * Main method for interaction with game.
	 *
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param action Performed action
	 *
	 * @see Action
	 */
	public void doActionOnSquare(int x, int y, Action action) {
		if (status == Status.LOSE || status == Status.WIN) return;
		if (!firstClick) { setNewBoard(x, y); firstClick = true; }

		if (!isInBoard(x, y)) return;

		try {
			Square s = getSquare(x, y);
			assert s != null;

			switch (action){
				case REVEAL:
					revealSquare(s);
					break;
				case FLAG:
					if (s.isChecked()) return;
					s.alternateFlag();
					break;
				case CHECK_FLAG:
					if (s.isChecked() && s.getNumberOfNearbyMines() == checkAdjacentSquaresForFlags(s))
						revealAdjacentSquares(s);
					break;
			}
		} catch (Exception e) {
			System.out.println("Exception during doing action on square!");
		}
	}

	/** Method that reveal Square after initial checks. */
	private void revealSquare(Square square){
		if (square.isChecked()) return;
		if (square.haveFlag()) return;

		if (square.haveMine()) {
			status = Status.LOSE;
			square.setChecked();
			notifyObservers();
			return;
		}

		square.setChecked();
		revealedSquares++;

		if(checkWinCondition()) { status = Status.WIN; notifyObservers(); }

		if (square.getNumberOfNearbyMines() == 0) revealAdjacentSquares(square);
	}

	/** Method that do check adjacent squares for number of flags.
	 * @param square square for check
	 *
	 * @return number of flags
	 *
	 * @see Square
	 */
	private int checkAdjacentSquaresForFlags(Square square){
		int i = 0;
		for (Square s: square.getAdjacentSquares()){
			if (s.haveFlag()) i++;
		}
		return i;
	}

	/** Method that reveals adjacent squares. */
	private void revealAdjacentSquares(Square square){
		for (Square s: square.getAdjacentSquares()){
			revealSquare(s);
		}
	}

	// Observer pattern.
	/** List of observers. */
	private List<Observer> observers = new ArrayList<>();

	/**
	 * Implementation of Subject for game.
	 * @see Subject
	 */
	@Override
	public void registerObserver(Observer observer) {
		this.observers.add(observer);
	}

	/**
	 * Implementation of Subject for game.
	 * @see Subject
	 */
	@Override
	public void unregisterObserver(Observer observer) {
		this.observers.remove(observer);
	}

	/**
	 * Implementation of Subject for game.
	 * @see Subject
	 */
	@Override
	public void notifyObservers() {
		for (Observer o: observers){
			o.updateGameStatus(status);
		}
	}
}
