package saperjava.model.observer;

import saperjava.model.Game;
import saperjava.model.Square;

/**
 * Part of observer for use by controller to receive updates from model.
 */
public interface Observer
{
	void updateGameStatus(Game.Status status);
	void updateSquareStatus(int x, int y, Square.Status status, int n);
}
