package saperjava.model.observer;

/**
 * Part of observer used by model to sent updates to controller.
 */
public interface Subject
{
	void registerObserver(Observer observer);
	void unregisterObserver(Observer observer);
	void notifyObservers();
}
