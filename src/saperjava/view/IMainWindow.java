package saperjava.view;

import saperjava.model.Square;
import java.util.List;

/**
 * Interface GUI used by controller.
 */
public interface IMainWindow {

	void createNewGame(int rows, int cols);
	void changeSquare(int x, int y, Square.Status status, int n);

	void setTimerTime(int t);
	void setMinesText(int m);

	String winWindow();
	void loseWindow();

	void showLeaderboard(List<LeaderboardInfo> infos);

	/**
	 * Internal class that is template for leaderboard information.
	 */
	class LeaderboardInfo{
		private String name;
		private String size;
		private int time;

		/**
		 * @return Name of player.
		 */
		String getName() {
			return name;
		}

		/**
		 * @return Size of game.
		 */
		String getSize() {
			return size;
		}

		/**
		 * @return Time of game.
		 */
		int getTime() {
			return time;
		}

		/**
		 * Constructor for creating information for leaderboard.
		 *
		 * @param name Name of player.
		 * @param size Size of game.
		 * @param time time of game.
		 */
		public LeaderboardInfo(String name, String size, int time){
			this.name = name;
			this.size = size;
			this.time = time;
		}
	};
}
