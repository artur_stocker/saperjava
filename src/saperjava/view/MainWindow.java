package saperjava.view;

import saperjava.controller.IGameController;
import saperjava.model.GameSettings;
import saperjava.model.Square;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/** Template for GUI object. */
public class MainWindow extends JFrame implements IMainWindow
{
	/** Reference to GameController object. */
	private IGameController gameController;

	/** Constructor creating object and setting reference to game controller and invoking gui.
	 * Start loading graphic.
	 *
	 * @param gameController GameController object that implements IGameController.
	 *
	 * @see IGameController
	 */
	public MainWindow(IGameController gameController) {
		super("Java Saper Game");
		this.gameController = gameController;

		loadIcons();

		try
		{
			EventQueue.invokeAndWait(() -> {
				createMainWindow();
				createMenu();
			});
		}
		catch (InterruptedException ie)
		{
			System.out.println("InterruptedException during MainWindow building!");
		}
		catch (InvocationTargetException ite)
		{
			System.out.println("InvocationTargetException during MainWindow building!");
		}
	}

	/** Reference to GamePanel object. */
	private GamePanel gamePanel = new GamePanel();
	/** Reference to SquaresPanel object. */
	private SquaresPanel squaresPanel = new SquaresPanel();

	/** Method that setting the main window view. */
	private void createMainWindow(){
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new JWindowHandler(this));

		setLocation(10,10);
		setResizable(false);

		add(gamePanel, "North");
		add(squaresPanel, "Center");

		pack();
		setVisible(true);
	}

	/** Method that create new representation of game.
	 *
	 * @param rows number of rows for new game
	 * @param cols number of cols for new game
	 *
	 * @see SquaresPanel
	 */
	@Override
	public void createNewGame(int rows, int cols) {
		squaresPanel.newGame(rows, cols);
		setGameButtonFace(GameButtonFace.SMILE);

		revalidate();
		repaint();
		pack();
	}


	/** Method that change view of square.
	 *
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param status desired status of square
	 * @param n number of nearby mines
	 *
	 * @see SquareView
	 */
	@Override
	public void changeSquare(int x, int y, Square.Status status, int n) {
		SquareView square = squaresPanel.getSquare(x, y);

		switch (status){
			case FLAG:
				square.setIcon(flag);
				square.setMargin(new Insets(0,0,0,0));
				break;

			case CLEAR:
				square.setIcon(null);
				break;

			case CHECKED:
				square.setBackground(Color.WHITE);

				if (n != 0) square.setText(String.format("%d",n));
				square.setMargin(new Insets(1,1,1,1));

				Color color;

				switch (n){
					case 1: color = new Color(6, 0, 243); break;
					case 2: color = new Color(9, 119, 8); break;
					case 3: color = new Color(247, 1, 5); break;
					case 4: color = new Color(7, 2, 129); break;
					case 5: color = new Color(131, 2, 3); break;
					case 6: color = new Color(0, 131, 127); break;
					case 7: color = new Color(2, 2, 2); break;
					case 8: color = new Color(126, 126, 126); break;
					default: color = Color.BLACK; break;
				}
				square.setForeground(color);
				break;

			case MINE:
				square.setIcon(mine);
				square.setMargin(new Insets(0,0,0,0));
				break;
		}
	}

	/** Reference to mine graphic. */
	private Icon mine;
	/** Reference to flag graphic. */
	private Icon flag;
	/** Reference to smile graphic. */
	private Icon smile;
	/** Reference to suprise graphic. */
	private Icon surprise;
	/** Reference to happy graphic. */
	private Icon happy;
	/** Reference to dead graphic. */
	private Icon dead;

	/** Method that sets references for graphic. */
	private void loadIcons(){
		try {
			mine = new ImageIcon(ImageIO.read(getClass().getResource("resources/mine.png")));
			flag = new ImageIcon(ImageIO.read(getClass().getResource("resources/flag.png")));
			smile = new ImageIcon(ImageIO.read(getClass().getResource("resources/smile.png")));
			surprise = new ImageIcon(ImageIO.read(getClass().getResource("resources/surprise.png")));
			happy = new ImageIcon(ImageIO.read(getClass().getResource("resources/happy.png")));
			dead = new ImageIcon(ImageIO.read(getClass().getResource("resources/dead.png")));
		} catch (IOException e) {
			System.out.println("IOException during loading icons!");
		}
	}

	/** Method that set timer label text. */
	@Override
	public void setTimerTime(int t) {
		int time = (t < 1000) ? t : 999;
		gamePanel.timerLabel.setText(String.format("Time: %03d", time));
	}

	/** Method that set mines label text. */
	@Override
	public void setMinesText(int m) {
		int mines = (m > -1000) ? ((m < 1000) ? m : 999) : -999;
		gamePanel.minesLabel.setText(String.format("Mines: % 04d", mines));
	}

	/** Method that shows win communicate that allows to git name.
	 * @return Name of player.
	 */
	@Override
	public String winWindow() {
		setGameButtonFace(GameButtonFace.HAPPY);
		String name = "";
		name = JOptionPane.showInputDialog(
				this,
				"Game Win! Write your name!"
		);
		disableSquares();
		return name;
	}

	/** Method that shows lose communicate. */
	@Override
	public void loseWindow() {
		setGameButtonFace(GameButtonFace.DEAD);
		JOptionPane.showMessageDialog(
				this,
				"Game Over!"
		);
		disableSquares();
	}

	/** Method that disable squares. */
	private void disableSquares(){
		for(var s: squaresPanel.squares) s.setEnabled(false);
	}

	/** Internal class for handling closing application. */
	private static class JWindowHandler extends WindowAdapter {
		/** Reference to JFrame object. */
		private JFrame frame;

		/** Constructor of object. Set reference to JFrame object. */
		private JWindowHandler(JFrame frame){
			this.frame = frame;
		}

		/** Method that handling closing event. Shows closing communicate. */
		public void windowClosing(WindowEvent event){
			if (JOptionPane.showConfirmDialog(frame,
					"Do you really want to close program?",
					"Close",
					JOptionPane.YES_NO_OPTION)
					== JOptionPane.OK_OPTION) System.exit(0);
		}
	}

	/** Method that setting menu bar. */
	private void createMenu(){
		JMenuBar mb = new JMenuBar();
		setJMenuBar(mb);

		JMenu gameMenu = new JMenu("Game");
		mb.add(gameMenu);

		JMenuItem newGame = new JMenuItem("New Game");
		gameMenu.add(newGame);	// -> new game with current game config
		newGame.addActionListener(e -> gameController.startNewGame());

		JMenuItem customGame = new JMenuItem("Custom Game");
		gameMenu.add(customGame);
		customGame.addActionListener(e -> startCustomGame());

		gameMenu.addSeparator();
		JMenuItem easyGame = new JMenuItem("Beginner");
		gameMenu.add(easyGame);
		easyGame.addActionListener(e -> gameController.startNewGame(beginnerGS));

		JMenuItem mediumGame = new JMenuItem("Intermediate");
		gameMenu.add(mediumGame);
		mediumGame.addActionListener(e -> gameController.startNewGame(intermediateGS));

		JMenuItem hardGame = new JMenuItem("Advanced");
		gameMenu.add(hardGame);
		hardGame.addActionListener(e -> gameController.startNewGame(advancedGS));

		gameMenu.addSeparator();
		JMenuItem exit = new JMenuItem("Exit");
		gameMenu.add(exit);
		exit.addActionListener(e -> System.exit(0));

		JMenu leaderMenu = new JMenu("Leaders");
		mb.add(leaderMenu);
		JMenuItem showLeaders = new JMenuItem("Show");
		leaderMenu.add(showLeaders);
		showLeaders.addActionListener(e -> gameController.showLeaderboard());

		JMenu helpMenu = new JMenu("Help");
		mb.add(helpMenu);
		JMenuItem about = new JMenuItem("About");
		helpMenu.add(about);
		about.addActionListener(e -> showAbout());
		JMenuItem controls = new JMenuItem("Controls");
		helpMenu.add(controls);
		controls.addActionListener(e -> showControls());

		pack();
	}

	/** Method that shows about communicate. */
	private void showAbout(){
		JOptionPane.showMessageDialog(
				this,
				"Saper Game v1.0\n" +
						"Autor: Artur Stocker"
				);
	}

	/** Method that shows controls communicate. */
	private void showControls(){
		JOptionPane.showMessageDialog(
				this,
				"Left mouse button >> reveal square\n" +
						"Rgiht mouse button >> place flag\n" +
						"Middle mouse button >> reveal nearby squres (if flags match)"
		);
	}

	/** Beginner GameSettings reference. */
	private GameSettings beginnerGS = new GameSettings.Builder()
			.setRows(9)
			.setCols(9)
			.setMines(10)
			.build();

	/** Intermediate GameSettings reference. */
	private GameSettings intermediateGS = new GameSettings.Builder()
			.setRows(16)
			.setCols(16)
			.setMines(40)
			.build();

	/** Advanced GameSettings reference. */
	private GameSettings advancedGS = new GameSettings.Builder()
			.setRows(16)
			.setCols(30)
			.setMines(99)
			.build();


	/** Method that shows custom game creator window. */
	private void startCustomGame() {
		GameSettings gs = new GameSettings();

		JTextField rows = new JTextField(5);
		JTextField cols = new JTextField(5);
		JTextField mines = new JTextField(5);

		JLabel numberOfMines = new JLabel(String.format(
				"Set number of mines (%d-%d)",
				GameSettings.MIN_MINES,
				gs.calcMaxMines()
		));

		// Make rows text field to update number of mines text
		rows.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {}

			@Override
			public void focusLost(FocusEvent e) {
				gs.setRows(convertToNumber(rows.getText()));
				numberOfMines.setText(String.format(
						"Set number of mines (%d-%d)",
						GameSettings.MIN_MINES,
						gs.calcMaxMines()
						)
				);
			}
		});

		// Make cols text field to update number of mines text
		cols.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {}

			@Override
			public void focusLost(FocusEvent e) {
				gs.setCols(convertToNumber(cols.getText()));
				numberOfMines.setText(String.format(
						"Set number of mines (%d-%d)",
						GameSettings.MIN_MINES,
						gs.calcMaxMines()
						)
				);
			}
		});

		// Create custom menu panel.
		JPanel menu = new JPanel();
		menu.setLayout(new BoxLayout(menu, BoxLayout.Y_AXIS));
		menu.add(new JLabel(String.format("Set number of rows (%d-%d)", GameSettings.MIN_ROWS, GameSettings.MAX_ROWS)));
		menu.add(rows);
		menu.add(new JLabel(String.format("Set number of cols (%d-%d)", GameSettings.MIN_COLS, GameSettings.MAX_COLS)));
		menu.add(cols);
		menu.add(numberOfMines);
		menu.add(mines);

		// Show custom menu.
		int result = JOptionPane.showConfirmDialog(
				this,
				menu,
				"Set custom game",
				JOptionPane.OK_CANCEL_OPTION
		);

		// Ok option.
		if (result == JOptionPane.OK_OPTION) {
			gs.setRows(convertToNumber(rows.getText()));
			gs.setCols(convertToNumber(cols.getText()));
			gs.setMines(convertToNumber(mines.getText()));

			// Dialog to confirm for user.
			JOptionPane.showMessageDialog(
					this,
					String.format(
							"Game will have size of: %dx%d, mines: %d",
							gs.getRows(),
							gs.getCols(),
							gs.getMines()
					)
			);

			gameController.startNewGame(gs);
		}
	}

	/** Method that convert text to number.
	 * @param text text to convert
	 * @return 0 if text wasn't a number
	 */
	private int convertToNumber(String text){
		try{
			return Integer.parseInt(text);
		} catch (NumberFormatException e){
			return 0;
		}
	}

	/** Internal class that is template of GamePanel. */
	private class GamePanel extends JPanel
	{
		/** Reference to timer label. */
		JLabel timerLabel = new JLabel("", JLabel.LEFT);
		/** Reference to new game button. */
		JButton newGameButton = new JButton("");
		/** Reference to mines label. */
		JLabel minesLabel = new JLabel("", JLabel.RIGHT);

		/** Constructor that sets game panel visuals. */
		GamePanel(){
			setLayout(new FlowLayout(FlowLayout.CENTER, 8, 2));
			add(timerLabel);
			add(newGameButton);
			newGameButton.setPreferredSize(new Dimension(25, 25));
			newGameButton.addActionListener(e -> gameController.startNewGame());
			newGameButton.setMargin(new Insets(0,0,0,0));
			newGameButton.setFocusPainted(false);
			add(minesLabel);
		}
	}

	/** Enum - Game button graphic states. */
	private enum GameButtonFace {SMILE, SURPRISE, HAPPY, DEAD}

	/** Method that changes game button graphic.
	 * @param state Desired state of button.
	 */
	private void setGameButtonFace(GameButtonFace state){
		switch (state){
			case SMILE:
				gamePanel.newGameButton.setIcon(smile);
				break;
			case SURPRISE:
				gamePanel.newGameButton.setIcon(surprise);
				break;
			case HAPPY:
				gamePanel.newGameButton.setIcon(happy);
				break;
			case DEAD:
				gamePanel.newGameButton.setIcon(dead);
				break;
		}
	}

	/** Internal class that is template for SquaresPanel. */
	private class SquaresPanel extends JPanel
	{
		/** Square size in pixels. */
		int squareSize = 20;

		/** List of squares. */
		List<SquareView> squares = new ArrayList<SquareView>();

		/** Method that creates new squares and give them good constraints.
		 *
		 * @param rows number of rows
		 * @param cols number of columns
		 */
		void newGame(int rows, int cols) {
			removeOld();

			setLayout(new GridBagLayout());
			setPreferredSize(new Dimension(squareSize * cols, squareSize * rows));

			GridBagConstraints c = new GridBagConstraints();
			for (int i = 0; i < cols; i++){
			for (int j = 0; j < rows; j++){
				// Creating new square.
				SquareView s = new SquareView(i+1, j+1);

				// Setting look of square.
				Dimension size = new Dimension(squareSize,squareSize);
				s.setMinimumSize(size);
				s.setMaximumSize(size);
				s.setFocusPainted(false);

				// Add squares to list.
				squares.add(s);

				c.gridx = i;
				c.gridy = j;
				add(s, c);
			}}

		}

		/** Method that clears list of squares. */
		void removeOld(){
			if (squares.isEmpty()) return;

			for (var s: squares)
			{
				remove(s);
			}
			squares.clear();
		}

		/** Method that return reference to square.
		 * @param x X coordinate
		 * @param y Y coordinate
		 * @return null if not find square with coordinates
		 */
		SquareView getSquare(int x, int y){
			for (SquareView s: squares){
				if (x == s.x && y == s.y) return s;
			}
			return null;
		}
	}

	/** Internal class that is template for SquareView. */
	private class SquareView extends JButton implements MouseListener
	{
		private int x;
		private int y;

		SquareView(int x, int y) {
			this.addMouseListener(this);
			this.x = x;
			this.y = y;
		}

		@Override
		public void mouseClicked(MouseEvent e) {}

		@Override
		public void mousePressed(MouseEvent e) {
			setGameButtonFace(GameButtonFace.SURPRISE);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			setGameButtonFace(GameButtonFace.SMILE);

			// Checking if pointer is over button.
			if (!model.isRollover()) return;

			gameController.doSquareAction(x, y, e.getButton());
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
	}

	/** Method that shows leaderboard window.
	 *
	 * @param infos List of records.
	 */
	@Override
	public void showLeaderboard(List<LeaderboardInfo> infos) {
		LeaderboardPanel leaderboardPanel = new LeaderboardPanel();

		if (infos != null){
			for (var info: infos){
				leaderboardPanel.addData(info.getName(), info.getSize(), info.getTime());
			}
		}

		JOptionPane.showMessageDialog(
				this,
				leaderboardPanel,
				"Leaderboards",
				JOptionPane.PLAIN_MESSAGE
		);
	}

	/** Internal class that is template for LeaderboardPanel. */
	private class LeaderboardPanel extends JPanel
	{
		/** Reference to table model. */
		DefaultTableModel tableModel;

		/** Constructor. */
		LeaderboardPanel(){
			String[] header = {"Name", "Size", "Time"};
			tableModel = new DefaultTableModel(header,0){
				// Disable cells editing.
				@Override
				public boolean isCellEditable(int row, int column){
					return false;
				}
			};

			JTable table = new JTable();

			table.setModel(tableModel);
			table.setPreferredScrollableViewportSize(new Dimension(300, 300));
			table.setFillsViewportHeight(true);
			table.setAutoCreateRowSorter(true);

			add(new JScrollPane(table));
		}

		/** Method that add data to table model. */
		void addData(String name, String size, int time){
			tableModel.addRow(new Object[]{name, size, time});
		}
	}
}
