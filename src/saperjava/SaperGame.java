package saperjava;

import saperjava.controller.GameController;
import saperjava.model.Game;
import saperjava.view.MainWindow;

/**
 * Main class with main() method used to start application.
 */
public class SaperGame
{

    /**
     * Main method that creating main objects of application and giving them
     * correct references.
     *
     * @param args (unused)
     * @see GameController
     * @see Game
     * @see MainWindow
     */
    public static void main(String[] args)
    {
        GameController gameController = new GameController();

        Game game = new Game();
        gameController.setGame(game);

        MainWindow mainWindow = new MainWindow(gameController);
        gameController.setMainWindow(mainWindow);
    }
}
